package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private static int[][] fillMatrixWithZeros(int rows, int columns) {
        if (rows < 0 && columns < 0)
            throw new CannotBuildPyramidException();
        int[][] matrix = new int[rows][columns];
        for (int[] row : matrix) {
            Arrays.fill(row, 0);
        }
        return matrix;
    }

    public List<Integer> bubbleSortArrayList(List<Integer> list) {
        Integer temp;
        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < list.size() - 1; i++) {
                if (list.get(i).compareTo(list.get(i + 1)) > 0) {
                    temp = list.get(i);
                    list.set(i, list.get(i + 1));
                    list.set(i + 1, temp);
                    sorted = false;
                }
            }
        }
        return list;
    }


    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        for (Integer el : inputNumbers) {
            if (el == null)
                throw new CannotBuildPyramidException();
        }

        List<Integer> sorted;

        try {
            sorted = bubbleSortArrayList(inputNumbers);
            Collections.reverse(sorted);
        } catch (UnsupportedOperationException e) {
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();
        int count = 0;
        int rows = 1;
        int columns = 1;
        while (count < size) {
            count = count + rows;
            rows++;
            columns = columns + 2;
        }
        rows = rows - 1;
        columns = columns - 2;

        if (size != count)
            throw new CannotBuildPyramidException();

        int[][] matrix = fillMatrixWithZeros(rows, columns);

        boolean toBeFilled;
        int offset = 0;
        int listIndex = 0;
        int bottomRowDigits = columns / 2 + 1;
        int digitsInRow = bottomRowDigits;

        for (int i = rows - 1; i >= 0; i--) {
            toBeFilled = true;
            for (int j = matrix[i].length - 1 - offset; j >= 0; j--) {
                if (toBeFilled) {
                    matrix[i][j] = inputNumbers.get(listIndex);
                    listIndex++;
                    digitsInRow--;
                    if (digitsInRow == 0) break;
                }
                toBeFilled = !toBeFilled;
            }
            offset++;
            digitsInRow = bottomRowDigits - offset;
        }
        return matrix;
    }
}
