package com.tsystems.javaschool.tasks.calculator;

public enum LexemesType {
    VALUE, NEGATIVE_VALUE, LEFT_BRACKET, RIGHT_BRACKET, OP_PLUS, OP_MINUS, OP_MUL, OP_DIV, EOF;
}
