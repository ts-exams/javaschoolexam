package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exception.SomethingWentWrongException;

import java.util.ArrayList;
import java.util.List;

public class Calc {

    public List<Lexeme> lexemeAnalyze(String stringExpression) throws SomethingWentWrongException {
        if (stringExpression == null || stringExpression.equals(""))
            throw new SomethingWentWrongException();
        int position = 0;
        List<Lexeme> lexemes = new ArrayList<Lexeme>();
        char character = stringExpression.charAt(position);

        if (stringExpression.charAt(position) == '-') {
            StringBuilder expression = new StringBuilder();
            do {
                expression.append(character);
                position++;
                character = stringExpression.charAt(position);
            } while ((character <= '9' && character >= '0') | character == '.');
            lexemes.add(new Lexeme(expression.toString(), LexemesType.NEGATIVE_VALUE));
        }

        while (position < stringExpression.length()) {
            character = stringExpression.charAt(position);
            switch (character) {
                case '(':
                    lexemes.add(new Lexeme(character, LexemesType.LEFT_BRACKET));
                    position++;
                    break;
                case ')':
                    lexemes.add(new Lexeme(character, LexemesType.RIGHT_BRACKET));
                    position++;
                    break;
                case '+':
                    lexemes.add(new Lexeme(character, LexemesType.OP_PLUS));
                    position++;
                    break;
                case '-':
                    lexemes.add(new Lexeme(character, LexemesType.OP_MINUS));
                    position++;
                    break;
                case '*':
                    lexemes.add(new Lexeme(character, LexemesType.OP_MUL));
                    position++;
                    break;
                case '/':
                    lexemes.add(new Lexeme(character, LexemesType.OP_DIV));
                    position++;
                    break;
                default:
                    if ((character <= '9' && character >= '0') | character == '.') {
                        StringBuilder expression = new StringBuilder();
                        do {
                            expression.append(character);
                            position++;
                            if (position >= stringExpression.length()) {
                                break;
                            }
                            character = stringExpression.charAt(position);
                        } while ((character <= '9' && character >= '0') | character == '.');
                        lexemes.add(new Lexeme(expression.toString(), LexemesType.VALUE));
                    } else {
                        if (character != ' ') {
                            return null;
                        }
                        position++;
                    }
            }
        }
        lexemes.add(new Lexeme("", LexemesType.EOF));
        return lexemes;
    }

    /**
     * Refers to expr rule. Starts calculation
     *
     * @param lexemes
     * @return
     */
    public Number calculate(LexemeBuffer lexemes) throws SomethingWentWrongException {
        if (lexemes.lexemes == null)
            throw new SomethingWentWrongException();
        Lexeme lexeme = lexemes.next();
        if (lexeme.getType() == LexemesType.EOF) {
            return null;
        } else {
            lexemes.back();
            double result = plusMinus(lexemes);
            if (result % 1 == 0)
                return (int) result;
            return result;
        }
    }

    /**
     * Refers to plusminus rule. Handles factors to execute subtracting or summation
     *
     * @param lexemes
     * @return
     */
    public double plusMinus(LexemeBuffer lexemes) throws SomethingWentWrongException {
        Double value = multDiv(lexemes);
        while (true) {
            Lexeme lexeme = lexemes.next();
            switch (lexeme.getType()) {
                case OP_PLUS:
                    value += multDiv(lexemes);
                    break;
                case OP_MINUS:
                    value -= multDiv(lexemes);
                    break;
                default:
                    lexemes.back();
                    return value;
            }
        }
    }

    /**
     * Refers to multdiv rule. Handles factors to execute division or multiplication.
     *
     * @param lexemes
     * @return
     */
    public double multDiv(LexemeBuffer lexemes) throws SomethingWentWrongException {
        double value = factor(lexemes);
        while (true) {
            Lexeme lexeme = lexemes.next();
            switch (lexeme.getType()) {
                case OP_MUL:
                    value *= factor(lexemes);
                    break;
                case OP_DIV:
                    value /= factor(lexemes);
                    if (value == Double.POSITIVE_INFINITY)
                        throw new SomethingWentWrongException();
                    break;
                default:
                    lexemes.back();
                    return value;
            }
        }
    }

    /**
     * Refers to factor rule. Base operation, allocating numbers & parsing them to int.
     *
     * @param lexemes
     * @return
     */
    public Double factor(LexemeBuffer lexemes) throws SomethingWentWrongException {
        Lexeme lexeme = lexemes.next();
        switch (lexeme.getType()) {
            case VALUE:
            case NEGATIVE_VALUE:
                try {
                    return Double.parseDouble(lexeme.getValue());
                } catch (NumberFormatException e) {
                    throw new SomethingWentWrongException();
                }
            case LEFT_BRACKET:
                double value = plusMinus(lexemes);
                lexeme = lexemes.next();
                if (lexeme.getType() != LexemesType.RIGHT_BRACKET) {
                    throw new SomethingWentWrongException();
                }
                return value;
        }
        throw new SomethingWentWrongException();
    }
}
