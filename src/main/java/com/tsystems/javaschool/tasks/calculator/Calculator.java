package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.exception.SomethingWentWrongException;

import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Calc calc = new Calc();
        Number calculate;

        try {
            List<Lexeme> lexemes = calc.lexemeAnalyze(statement);
            LexemeBuffer lexemeBuffer = new LexemeBuffer(lexemes);
            calculate = calc.calculate(lexemeBuffer);
        } catch (SomethingWentWrongException e) {
            return null;
        }
        return String.valueOf(calculate);
        /*
         * expr : plusminus* EOF ;
         * plusminus: multdiv ( ( '+' | '-' ) multdiv )* ;
         * multdiv : factor ( ( '*' | '/' ) factor )* ;
         * factor : NUMBER | '(' expr ')' ;
         */
    }

}
