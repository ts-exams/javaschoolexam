package com.tsystems.javaschool.tasks.calculator;

import java.util.List;

public class LexemeBuffer {

    public List<Lexeme> lexemes;
    private int pos;

    public LexemeBuffer(List<Lexeme> lexemes) {
        this.lexemes = lexemes;
    }

    public Lexeme next() {
        return lexemes.get(pos++);
    }

    public void back() {
        pos--;
    }

    public int getPos() {
        return pos;
    }

}
