package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null | y == null)
            throw new IllegalArgumentException();

        int sizeX = x.size();
        int sizeY = y.size();

        if (sizeX > sizeY)
            return false;

        if (sizeX == 0 && sizeY == 0)
            return true;

        List resultList = Arrays.asList(new Object[sizeX]);
        int yOrder = 0;

        for (int i = 0; i < sizeX; i++) {
            for (int j = 0; j < sizeY; j++) {
                if (x.get(i).equals(y.get(j)) && i <= j && yOrder <= j) {
                    if (!resultList.contains(y.get(j))) {
                        yOrder = j;
                        resultList.set(i, y.get(j));
                    }
                }
            }
        }

        for (int i = 0; i < sizeX; i++) {
            if (!x.get(i).equals(resultList.get(i)))
                return false;
        }
        return true;
    }
}
